import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GinoPageRoutingModule } from './gino-routing.module';

import { GinoPage } from './gino.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GinoPageRoutingModule
  ],
  declarations: [GinoPage]
})
export class GinoPageModule {}
