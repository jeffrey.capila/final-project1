import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GinoPage } from './gino.page';

const routes: Routes = [
  {
    path: '',
    component: GinoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GinoPageRoutingModule {}
