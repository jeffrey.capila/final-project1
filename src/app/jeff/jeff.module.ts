import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { JeffPageRoutingModule } from './jeff-routing.module';

import { JeffPage } from './jeff.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    JeffPageRoutingModule
  ],
  declarations: [JeffPage]
})
export class JeffPageModule {}
