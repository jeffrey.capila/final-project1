import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'search',
    loadChildren: () => import('./search/search.module').then( m => m.SearchPageModule)
  },
  {
    path: 'befsearch',
    loadChildren: () => import('./befsearch/befsearch.module').then( m => m.BefsearchPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'detail',
    loadChildren: () => import('./detail/detail.module').then( m => m.DetailPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./about/about.module').then( m => m.AboutPageModule)
  },
  {
    path: 'gino',
    loadChildren: () => import('./gino/gino.module').then( m => m.GinoPageModule)
  },
  {
    path: 'jeff',
    loadChildren: () => import('./jeff/jeff.module').then( m => m.JeffPageModule)
  },
  {
    path: 'jm',
    loadChildren: () => import('./jm/jm.module').then( m => m.JmPageModule)
  },
  {
    path: 'exit',
    loadChildren: () => import('./exit/exit.module').then( m => m.ExitPageModule)
  },
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
