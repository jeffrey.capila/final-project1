import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JeffPage } from './jeff.page';

const routes: Routes = [
  {
    path: '',
    component: JeffPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JeffPageRoutingModule {}
