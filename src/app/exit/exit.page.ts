import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AlertController, LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-exit',
  templateUrl: './exit.page.html',
  styleUrls: ['./exit.page.scss'],
})
export class ExitPage {

  subscribe:any;
  constructor( private router: Router,    private alertController: AlertController,) { }
  async signOut() {
    const toast = await this. alertController.create({
      message:'Logout successfully',
   
    
    });
 
    toast.present();
      this.router.navigateByUrl('/login', { replaceUrl: true });
      
  
    
  }
  
  ngOnInit() {
  }

}