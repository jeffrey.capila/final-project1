import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BefsearchPage } from './befsearch.page';

const routes: Routes = [
  {
    path: '',
    component: BefsearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BefsearchPageRoutingModule {}
