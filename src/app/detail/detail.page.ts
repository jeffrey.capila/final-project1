import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  placesId;

  places: any = {};

  constructor(
    private route: ActivatedRoute
  ) { 
    // this.placesId = this.route.snapshot.paramMap.get('xxx');
    this.route.queryParams.subscribe( res =>{
      console.log(res);
      this.places = res;
    })
  }

  ngOnInit() {
  }

}
