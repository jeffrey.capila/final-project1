import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  searchTerm: string;
  places = [
    {
      "name": "Beach",
    },
    {
      "name": "Church",
    },
    {
      "name": "Mountain",
    },
    {
      "name": "Resort",
    },
    {
      "name": "Place",
    },
    {
      "name": "Sightseeing",
    },
    {
      "name": "Amusement Park",
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}

