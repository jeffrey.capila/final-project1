import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BefsearchPageRoutingModule } from './befsearch-routing.module';

import { BefsearchPage } from './befsearch.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BefsearchPageRoutingModule
  ],
  declarations: [BefsearchPage]
})
export class BefsearchPageModule {}
