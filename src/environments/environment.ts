// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:  {
    apiKey: "AIzaSyDcYGhT-PdRXj_WQKvM4Wv9N35iDUi_F0Y",
    authDomain: "travel-login-8ad62.firebaseapp.com",
    projectId: "travel-login-8ad62",
    storageBucket: "travel-login-8ad62.appspot.com",
    messagingSenderId: "502886227402",
    appId: "1:502886227402:web:6dedd5a0d4f316c07f8333"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
